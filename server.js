#!/usr/bin/env node

const fs = require('fs')
const path = require('path')
const http = require('http')
const express = require('express')
const cors = require('cors')
const ejs = require('ejs')
const cons = require('consolidate')
const passport = require('passport')
const compression = require('compression')
const utils = require('./services/utils')
const pm2 = require('./services/pm2-utils')
const basicAuth = require('express-basic-auth')
const bodyParser = require('body-parser')
const { CONFIG } = require('./services/config')
const authentication = require('./services/authentication')
const passportService = require('./services/passport')

const app = express()

/** SETUP EXPRESS */

app.use(compression())
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Credentials', true)
  res.header('Access-Control-Allow-Origin', req.headers.origin)
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  res.header(
    'Access-Control-Allow-Headers',
    'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept'
  )
  if (req.method === 'OPTIONS') {
    res.send(200)
  } else {
    next()
  }
})

app.use(cors())
app.engine('html', cons.swig)
app.set('view engine', 'ejs')
app.set('views', __dirname + '/views')
app.use(express.static(path.resolve(__dirname, 'public')))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

/** REGISTER ROUTES */

app.get('/', (req, res) => {
  res.render('index')
})
app.get('/login', (req, res) => {
  res.render('login')
})
app.get('/network', (req, res) => {
  res.render('network')
})

const requireAuth = passport.authenticate('jwt', { session: false })
const requireSignin = passport.authenticate('local', { session: false })

app.post('/api/signin', requireSignin, authentication.signin)

app.post('/api/changepass', requireAuth, authentication.changepass)

app.get('/api/node/config', requireAuth, async (req, res) => {
  const config = utils.getConfig()
  return res.json(config)
})

app.post('/api/node/config', requireAuth, async (req, res) => {
  const config = req.body
  utils.setConfig(config)
  return res.json({ success: true })
})

app.get('/api/node/env', requireAuth, async (req, res) => {
  const config = utils.getEnv()
  return res.json(config)
})

app.post('/api/node/env', requireAuth, async (req, res) => {
  const env = req.body
  utils.setEnv(env)
  return res.json({ success: true })
})

app.get('/api/node/status', requireAuth, async (req, res) => {
  const status = await utils.status()
  return res.json(status)
})

let logStream
let logConnection

app.get('/api/node/log', requireAuth, async (req, res) => {
  // Close the last open connection
  if (logConnection) logConnection.end()
  // Save the new connection
  logConnection = res
  // Create a log stream if it doesnt exist
  if (!logStream) logStream = utils.log()
  // Tell the new connection to stay open and expect Server-sent Events
  logConnection.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    Connection: 'keep-alive'
  })
  // Set the log stream to send its data on the new connection
  logStream.removeAllListeners('data')
  logStream.on('data', chunk => {
    const str = JSON.stringify(chunk.toString())
    logConnection.write('data: ' + str + '\n\n')
    logConnection.flush()
  })
})

app.post('/api/node/start', requireAuth, async (req, res) => {
  await utils.start()
  return res.json({ success: true })
})

app.post('/api/node/stop', requireAuth, async (req, res) => {
  await utils.stop()
  return res.json({ success: true })
})

app.post('/api/geolocation', requireAuth, async (req, res) => {
  let ipArr = req.body.ip
  let locations = utils.mapIpToLocation(ipArr)
  return res.json({ success: true, locations })
})

/** START SERVER */

const httpServer = http.createServer(app)
httpServer.listen(CONFIG.port, err => {
  if (err) console.log(err)
  console.log(`HTTP server is listening at port ${CONFIG.port}`)
})
