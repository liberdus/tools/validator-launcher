# Libderus Validator Launcher

A launcher that provides an admin dashboard to manage Liberdus validator nodes.

## Usage

```bash
$ server.js path/to/basedir path/to/liberdus/index.js
```

The launcher serves a dashboard at `localhost:9999` that lets the user manage a Liberdus validator node.

Default login:
```
username: admin
password: liberdus
```

The node will use the provided `path/to/basedir` as its base directory.
