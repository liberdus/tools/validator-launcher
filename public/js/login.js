var app = new Vue({
  el: '#app',
  data: {
    form: {
      username: '',
      password: ''
    },
    loading: false
  },
  computed: {
    isFormValid: function () {
      return true
    }
  },
  mounted: async function () {},
  methods: {
    onSubmit: async function (e) {
      e.preventDefault()
      try {
        this.loading = true
        let res = await axios.post('/api/signin', this.form)
        console.log(res.data)
        if (res.data.token) {
          console.log('Login Success')
          localStorage.setItem('token', res.data.token)
          window.location.href = '/'
        } else {
          alert('Incorrect username or password')
        }
        this.loading = false
      } catch (e) {
        alert('Incorrect username or password')
      }
    }
  }
})
