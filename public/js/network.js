let greenIcon = new L.Icon({
  iconUrl:
    'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
  shadowUrl:
    'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
})
let yellowIcon = new L.Icon({
  iconUrl:
    'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-yellow.png',
  shadowUrl:
    'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
})
var app = new Vue({
  el: '#app',
  data: {
    cycleChain: null,
    currentCycle: null,
    starting: false,
    isNodeActive: false,
    activeNodeList: [],
    joined: {},
    ipToLocationMap: {},
    timer: { usage: null },
    usage: null,
    map: null,
    markers: {},
    syncMarkers: {},
    syncingCount: 0,
    activeCount: 0,
    seedListHost: 'http://localhost:4000'
  },
  computed: {
    nodeCount: function () {
      return this.activeNodeList.length
    }
  },
  mounted: async function () {
    console.log('Mounted...')
    try {
      this.token = localStorage.getItem('token')
    } catch (e) {
      console.warn(e)
      window.location.href = '/login'
    }
    if (!this.token) {
      window.location.href = '/login'
    }
    let res = await this.queryServer('/api/node/config')
    if (res.data.server) {
      let existingArchivers = res.data.server.p2p.existingArchivers
      this.seedListHost = `http://${existingArchivers[0].ip}:${existingArchivers[0].port}`
      console.log(`Seed List: ${this.seedListHost}`)
    }
    this.buildNetworkInfo()
    setInterval(this.updateNetworkInfo, 5000)
  },
  updated: function () {},
  methods: {
    queryServer: async function (url, opts) {
      let reqObj = {
        method: 'get',
        url: url,
        headers: {
          token: this.token
        },
        ...opts
      }
      try {
        let res = await axios(reqObj)
        return res
      } catch (err) {
        console.log(err)
        if (err.response.status === 401) {
          localStorage.removeItem('token')
          window.location.href = '/login'
        }
      }
    },
    postServer: async function (url, data) {
      let reqObj = {
        method: 'post',
        url: url,
        data,
        headers: {
          token: this.token
        }
      }
      try {
        let res = await axios(reqObj)
        return res
      } catch (err) {
        console.log(err)
        if (err.response.status === 401) {
          // localStorage.removeItem('token')
          // window.location.href = '/login'
        }
      }
    },

    buildNetworkInfo: async function () {
      let res = await this.queryServer(`${this.seedListHost}/cycleinfo/1`)
      if (res.data.cycleInfo) {
        this.currentCycle = res.data.cycleInfo[0]
        this.syncingCount = this.currentCycle.syncing
        this.activeCount = this.currentCycle.active
        let activeNodeCount = this.currentCycle.active
        let requiredCycleCount = Math.ceil(Math.sqrt(activeNodeCount))
        if (requiredCycleCount < this.activeCount)
          requiredCycleCount = this.activeCount
        this.activeNodeList = await this.buildActiveNodeList(requiredCycleCount)
        this.drawMap()
      }
    },
    updateNetworkInfo: async function () {
      let res = await this.queryServer(`${this.seedListHost}/cycleinfo/1`)
      if (res.data.cycleInfo) {
        this.currentCycle = res.data.cycleInfo[0]
        this.syncingCount = this.currentCycle.syncing
        this.activeCount = this.currentCycle.active

        this.activeNodeList = await this.updateActiveNodeList(this.currentCycle)
        this.drawMap()
      }
    },
    buildActiveNodeList: async function (cycleCount) {
      let activeNodeMap = new Map()
      let activeNodeList = []
      const res = await this.queryServer(
        `${this.seedListHost}/cycleinfo/${cycleCount}`
      )
      if (res.data.cycleInfo) {
        const chain = res.data.cycleInfo
        for (let i = 0; i < chain.length; i++) {
          let refreshedConsensors = JSON.parse(chain[i].refreshedConsensors)
          for (let j = 0; j < refreshedConsensors.length; j++) {
            let node = refreshedConsensors[j]
            // console.log(node.id)
            if (!activeNodeMap.has(node.id)) {
              console.log(`Adding node ${node.id} to List`)
              // if (this.ipToLocationMap[node.externalIp]) {
              if (this.ipToLocationMap[node.id]) {
                node.geoLocation = this.ipToLocationMap[node.externalIp]
              } else {
                node.geoLocation = await this.getLocationFromIp([
                  node.externalIp
                ])
                if (!node.geoLocation) {
                  console.warn(`Unable to get geo location for ${node.id}`)
                }
                // this.ipToLocationMap[node.externalIp] = node.geoLocation
                this.ipToLocationMap[node.id] = node.geoLocation
              }
              activeNodeMap.set(node.id, node)
            }
          }
        }
        for (let i = 0; i < chain.length; i++) {
          let joinedConsensors = JSON.parse(chain[i].joinedConsensors)
          for (let j = 0; j < joinedConsensors.length; j++) {
            let node = joinedConsensors[j]
            if (!activeNodeMap.has(node.id)) {
              console.log(`Adding syncing node ${node.id} to Joined List`)
              // if (this.ipToLocationMap[node.externalIp]) {
              if (this.ipToLocationMap[node.id]) {
                node.geoLocation = this.ipToLocationMap[node.externalIp]
              } else {
                node.geoLocation = await this.getLocationFromIp([
                  node.externalIp
                ])
                if (!node.geoLocation) {
                  console.warn(`Unable to get geo location for ${node.id}`)
                }
                // this.ipToLocationMap[node.externalIp] = node.geoLocation
                this.ipToLocationMap[node.id] = node.geoLocation
              }
              this.joined[node.id] = node
            }
          }
        }
        return Array.from(activeNodeMap.values())
      }
    },
    updateActiveNodeList: async function (currentCycle) {
      let activeNodeMap = new Map(this.activeNodeList.map(n => [n.id, n]))
      let joined = JSON.parse(currentCycle.joinedConsensors)
      let activated = JSON.parse(currentCycle.activated)
      let refreshed = JSON.parse(currentCycle.refreshedConsensors)
      let removed = JSON.parse(currentCycle.removed)
      let lost = JSON.parse(currentCycle.lost)
      let returned = JSON.parse(currentCycle.returned)
      let apoptosized = JSON.parse(currentCycle.apoptosized)
      // Add joined nodes to joined list
      for (let j = 0; j < joined.length; j++) {
        let node = joined[j]
        if (!this.joined[node.id]) {
          if (this.ipToLocationMap[node.externalIp]) {
            // TODO: to change node.externalIp
            node.geoLocation = this.ipToLocationMap[node.id]
          } else {
            node.geoLocation = await this.getLocationFromIp([node.externalIp])
            // TODO: to change node.externalIp
            this.ipToLocationMap[node.id] = node.geoLocation
          }
          console.log(`Adding a syncing node: ${node.id}`)
          this.joined[node.id] = node
        }
      }
      // Add refreshed nodes to active list
      for (let j = 0; j < refreshed.length; j++) {
        let node = refreshed[j]
        if (this.ipToLocationMap[node.externalIp]) {
          // TODO: to change node.externalIp
          node.geoLocation = this.ipToLocationMap[node.id]
        } else {
          node.geoLocation = await this.getLocationFromIp([node.externalIp])
          // TODO: to change node.externalIp
          this.ipToLocationMap[node.id] = node.geoLocation
        }

        if (!activeNodeMap.has(node.id)) {
          console.log(`Adding a refreshed node: ${node.id}`)
          activeNodeMap.set(node.id, node)
        }
      }
      // Add activated nodes to this.activeNodeList
      for (let j = 0; j < activated.length; j++) {
        let nodeId = activated[j]
        if (this.joined[nodeId]) {
          console.log(`Setting syncing node ${nodeId} to active node...`)
          if (!activeNodeMap.has(nodeId)) {
            activeNodeMap.set(nodeId, this.joined[nodeId])
            delete this.joined[nodeId]
          }
        }
      }
      // Delete removed nodes from list
      for (let j = 0; j < removed.length; j++) {
        let nodeId = removed[j]
        if (activeNodeMap.get(nodeId)) {
          console.log(`Rotated: Removing node ${nodeId} from map`)
          activeNodeMap.delete(nodeId)
        }
      }
      // Delete lost nodes from list
      for (let j = 0; j < lost.length; j++) {
        let nodeId = lost[j]
        if (activeNodeMap.get(nodeId)) {
          console.log(`Lost: Removing node ${nodeId} from map`)
          activeNodeMap.delete(nodeId)
        }
      }
      // Delete apoptosized nodes from list
      for (let j = 0; j < apoptosized.length; j++) {
        let nodeId = apoptosized[j]
        if (activeNodeMap.get(nodeId)) {
          console.log(`Apoptosized: Removing node ${nodeId} from map`)
          activeNodeMap.delete(nodeId)
        }
      }
      return Array.from(activeNodeMap.values())
    },
    getLocationFromIp: async function (ipArr) {
      let res = await this.postServer(`/api/geolocation`, {
        ip: ipArr
      })
      if (res.data && res.data.success) return res.data.locations[0]
      else return null
    },
    initMap: function () {
      let mapOptions = {
        center: [0, 0],
        zoom: 2
      }
      this.map = L.map('mapid').setView(mapOptions.center, mapOptions.zoom)
      var layer = new L.TileLayer(
        'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
      )
      this.map.addLayer(layer)
    },
    drawMarkers: function () {
      let activeMarkersCollector = {}
      let syncingMarkersCollector = {}
      for (let i = 0; i < this.activeNodeList.length; i++) {
        let node = this.activeNodeList[i]
        if (this.markers[node.id]) {
          // console.log(`Marker already existed for ${node.id}`)
        } else {
          let marker = L.marker(
            [
              node.geoLocation.location.latitude,
              node.geoLocation.location.longitude
            ],
            { icon: greenIcon }
          ).addTo(this.map)
          marker.bindPopup(
            `<b>${node.id.slice(0, 6)}...${node.id.slice(58)}</b>
            <br>
            Host: ${node.externalIp}:${node.externalPort}</b>
            <br>
            Location: ${
              node.geoLocation.city ? `${node.geoLocation.city}, ` : ''
            } ${node.geoLocation.country}
            `
          )
          activeMarkersCollector[node.id] = marker
        }
      }
      for (let nodeId in this.joined) {
        let node = this.joined[nodeId]
        if (this.syncMarkers[node.id]) {
          // console.log(`Marker already existed for ${node.id}`)
        } else {
          let marker = L.marker(
            [
              node.geoLocation.location.latitude,
              node.geoLocation.location.longitude
            ],
            { icon: yellowIcon }
          ).addTo(this.map)
          marker.bindPopup(
            `<b>${node.id.slice(0, 6)}...${node.id.slice(58)}</b>
            <br>
            Host: ${node.externalIp}:${node.externalPort}</b>
            <br>
            Location: ${
              node.geoLocation.city ? `${node.geoLocation.city}, ` : ''
            } ${node.geoLocation.country}
            `
          )
          syncingMarkersCollector[node.id] = marker
        }
      }
      this.markers = { ...this.markers, ...activeMarkersCollector }
      this.syncMarkers = { ...this.syncMarkers, ...syncingMarkersCollector }
    },
    clearMarkers: function () {
      for (let nodeId in this.markers) {
        let isNodeStillActive = this.activeNodeList.some(n => n.id === nodeId)
        if (!isNodeStillActive) {
          console.log(`Removing active marker for ${nodeId}`)
          this.map.removeLayer(this.markers[nodeId])
          delete this.markers[nodeId]
        }
      }
      for (let nodeId in this.syncMarkers) {
        if (!this.joined[nodeId]) {
          console.log(`Removing syncing marker for ${nodeId}`)
          this.map.removeLayer(this.syncMarkers[nodeId])
          delete this.syncMarkers[nodeId]
        }
      }
    },
    drawMap: async function () {
      if (!this.map) {
        this.initMap()
      }
      this.clearMarkers()
      this.drawMarkers()
    },
    onShowLog: async function (e) {
      e.preventDefault()
      this.showLog = true
    },
    onHideLog: async function (e) {
      e.preventDefault()
      this.showLog = false
    },
    onScrollLog: function (e) {
      // Save whether the log was at the bottom
      this.logAtBottom = log.scrollTop + log.offsetHeight >= log.scrollHeight
    }
  }
})
