var app = new Vue({
  el: '#app',
  data: {
    form: {
      externalIp: '',
      externalPort: 0,
      internalIp: '',
      internalPort: 0,
      archiverIp: '',
      archiverPort: 0,
      archiverPublicKey: '',
      monitorReportUrl: '',
      payAddress: '',
    },
    starting: false,
    isNodeActive: false,
    logSource: null,
    log: '',
    showLog: false,
    logAtBottom: true,
    timer: { usage: null },
    usage: null,
  },
  computed: {
    isFormValid: function () {
      return true
    },
  },
  mounted: async function () {
    // Goto login page if we dont have a token
    try {
      let token = localStorage.getItem('token')
      this.token = token
    } catch (e) {
      console.warn(e)
      window.location.href = '/login'
    }
    if (!this.token) {
      window.location.href = '/login'
    }

    // Fill out form with existing node settings
    this.getSettings()

    // Get a log feed as Server-sent events from the node
    this.logSource = new EventSourcePolyfill('/api/node/log', {
      headers: {
        token: this.token,
      },
    })
    this.logSource.onmessage = (e) => {
      this.log += JSON.parse(e.data)
    }

    // Get the nodes status on an interval
    await this.updateNodeStatus()
    if (!this.timer.usage) {
      this.timer.usage = setInterval(() => {
        this.updateNodeStatus()
      }, 10000)
    }
  },
  updated: function () {
    this.$nextTick(function () {
      // Autoscroll log on update if it was at the bottom
      const log = document.getElementById('log')
      if (log && this.logAtBottom) {
        log.scrollTop = log.scrollHeight
      }
    })
  },
  methods: {
    queryServer: async function (url, opts) {
      let reqObj = {
        method: 'get',
        url: url,
        headers: {
          token: this.token,
        },
        ...opts,
      }
      try {
        let res = await axios(reqObj)
        return res
      } catch (err) {
        console.log(err)
        if (err.response.status === 401) {
          localStorage.removeItem('token')
          window.location.href = '/login'
        }
      }
    },
    postServer: async function (url, data) {
      let reqObj = {
        method: 'post',
        url: url,
        data,
        headers: {
          token: this.token,
        },
      }
      try {
        let res = await axios(reqObj)
        return res
      } catch (err) {
        console.log(err)
        if (err.response.status === 401) {
          // localStorage.removeItem('token')
          // window.location.href = '/login'
        }
      }
    },
    getSettings: async function () {
      const configRes = await this.queryServer('/api/node/config')
      const config = configRes.data
      const envRes = await this.queryServer('/api/node/env')
      const env = envRes.data
      this.form.externalIp = config.server.ip.externalIp
      this.form.externalPort = config.server.ip.externalPort
      this.form.internalIp = config.server.ip.internalIp
      this.form.internalPort = config.server.ip.internalPort
      this.form.archiverIp = config.server.p2p.existingArchivers[0].ip
      this.form.archiverPort = config.server.p2p.existingArchivers[0].port
      this.form.archiverPublicKey = config.server.p2p.existingArchivers[0].publicKey
      this.form.monitorReportUrl = config.server.reporting.recipient
      this.form.payAddress = env['PAY_ADDRESS']
    },
    setSettings: async function () {
      const config = {
        server: {
          p2p: {
            existingArchivers: [
              {
                ip: this.form.archiverIp,
                port: this.form.archiverPort,
                publicKey: this.form.archiverPublicKey,
              },
            ],
          },
          ip: {
            externalIp: this.form.externalIp,
            externalPort: parseInt(this.form.externalPort, 10) || this.form.externalPort,
            internalIp: this.form.internalIp,
            internalPort: parseInt(this.form.internalPort, 10) || this.form.internalPort,
          },
          reporting: {
            report: true,
            recipient: this.form.monitorReportUrl,
            interval: 2,
            console: false,
          },
        },
      }

      const env = {
        PAY_ADDRESS: this.form.payAddress,
      }

      await this.postServer('/api/node/config', config)
      await this.postServer('/api/node/env', env)
    },
    updateNodeStatus: async function () {
      let res = await this.queryServer(`/api/node/status`)
      if (res.data.status) {
        let pm2List = JSON.parse(res.data.status)
        let liberdusProcess = pm2List.find((l) => l.name === 'liberdus')
        if (liberdusProcess && liberdusProcess.pm2_env.status === 'online') {
          this.isNodeActive = true
          this.usage = {
            process: 'liberdus',
            cpu: liberdusProcess.monit.cpu,
            ram: parseInt(liberdusProcess.monit.memory) / 10 ** 6,
          }
        }
      }
    },
    onShowLog: async function (e) {
      e.preventDefault()
      this.showLog = true
    },
    onHideLog: async function (e) {
      e.preventDefault()
      this.showLog = false
    },
    onScrollLog: function (e) {
      // Save whether the log was at the bottom
      this.logAtBottom = log.scrollTop + log.offsetHeight >= log.scrollHeight
    },
    onStartNode: async function (e) {
      e.preventDefault()

      await this.setSettings()

      let res = await this.postServer('/api/node/start')
      if (res.data.success) {
        this.isNodeActive = true
        this.updateNodeStatus()
        if (!this.timer.usage) {
          this.timer.usage = setInterval(() => {
            this.updateNodeStatus()
          }, 10000)
        }
      }
    },
    onStopNode: async function (e) {
      e.preventDefault()
      console.log('Stopping node...')
      let res = await this.postServer('/api/node/stop', {})
      this.isNodeActive = false
    },
    changeAdminPass: async function (e) {
      e.preventDefault()
      const password = window.prompt('New admin password:')
      const res = await this.postServer('/api/changepass', {
        username: 'admin',
        password,
      })
      if (res && res.data && res.data.success) {
        const newToken = res.data.token
        localStorage.removeItem('token')
        localStorage.setItem('token', newToken)
        this.token = newToken
      } else {
        window.alert('Unable to change admin password.')
      }
    },
  },
})
