const fs = require('fs')
const path = require('path')
const pm2 = require('./pm2-utils')
const geoIP = require('offline-geo-from-ip')

const { CONFIG, BASE_DIR } = require('./config')
const NODE_DEFAULT_CONFIG = require('../config.default.json')
const NODE_DEFAULT_ENV = parseEnv(
  fs.readFileSync(path.resolve(__dirname, '../.env.default'), 'utf8')
)

/** LOAD OR CREATE NODE CONFIG IN BASE_DIR */

const NODE_CONFIG_PATH = path.resolve(BASE_DIR, 'config.json')

let NODE_CONFIG
try {
  NODE_CONFIG = JSON.parse(fs.readFileSync(NODE_CONFIG_PATH, 'utf8'))
  if (!NODE_CONFIG) throw new Error("'config.json' is empty or does not exist")
} catch (err) {
  const msg = err.message || err
  console.error(msg)
  console.log("Creating new 'config.json'...")
  fs.writeFileSync(
    NODE_CONFIG_PATH,
    JSON.stringify(NODE_DEFAULT_CONFIG, null, 2)
  )
  NODE_CONFIG = JSON.parse(fs.readFileSync(NODE_CONFIG_PATH, 'utf8'))
}

const NODE_ENV_PATH = path.resolve(BASE_DIR, '.env')

let NODE_ENV
try {
  NODE_ENV = parseEnv(fs.readFileSync(NODE_ENV_PATH, 'utf8'))
  if (!NODE_ENV) throw new Error("'.env' is empty or does not exist")
} catch (err) {
  const msg = err.message || err
  console.error(msg)
  console.log("Creating new '.env'...")
  fs.writeFileSync(NODE_ENV_PATH, stringifyEnv(NODE_DEFAULT_ENV))
  NODE_ENV = parseEnv(fs.readFileSync(NODE_ENV_PATH, 'utf8'))
}

async function ensurePm2Daemon () {
  const list = await pm2.list()
  console.log(list)
}
ensurePm2Daemon()

exports.getConfig = function () {
  return NODE_CONFIG
}

exports.setConfig = function (config) {
  NODE_CONFIG = config
  fs.writeFileSync(NODE_CONFIG_PATH, JSON.stringify(NODE_CONFIG, null, 2))
}

exports.getEnv = function () {
  return NODE_ENV
}

exports.setEnv = function (env) {
  NODE_ENV = env
  fs.writeFileSync(NODE_ENV_PATH, stringifyEnv(NODE_ENV, null, 2))
}

exports.status = async function () {
  const list = await pm2.jlist()
  return { status: list }
}

exports.log = function () {
  const stream = pm2.log('liberdus')
  return stream
}

exports.start = async function () {
  const pm2Args = ['pm2--count-exit-errors', 'pm2--max-restarts=1']
  await pm2.flush()
// Omar changed the following line so that the PAY_ADDRESS entered by the user in the admin UI is picked up
//  await pm2.start(CONFIG.liberdus, 'liberdus', { BASE_DIR }, pm2Args)
  await pm2.start(CONFIG.liberdus, 'liberdus', { BASE_DIR, PAY_ADDRESS: NODE_ENV.PAY_ADDRESS }, pm2Args)
}

exports.stop = async function () {
  await pm2.stop('liberdus')
}

exports.mapIpToLocation = function (ipArr) {
  console.log(`mapping IP to location...`)
  let randomIpForTesting = [
    '94.80.244.59',
    '30.10.56.178',
    '56.150.177.207',
    '6.69.15.58',
    '147.145.46.43',
    '31.5.97.147',
    '24.206.115.249',
    '109.15.233.104',
    '1.147.103.117',
    '131.111.189.136',
    '125.200.100.244',
    '147.113.101.130',
    '149.238.213.110',
    '120.75.249.193',
    '95.171.107.157',
    '169.35.24.209',
    '74.207.233.118',
    '173.255.195.137',
    '210.14.96.174',
    '167.172.28.192'
  ]
  let locations = ipArr.map(ip => {
    if (ip === '127.0.0.1') {
      let randIndex = Math.floor(Math.random() * randomIpForTesting.length)
      let randIp = randomIpForTesting[randIndex]
      let data = geoIP.allData(randIp)
      return data
    }
    return geoIP.allData(ip)
  })
  return locations
}

exports.NODE_CONFIG = NODE_CONFIG
exports.NODE_ENV = NODE_ENV

function parseEnv (str) {
  const obj = {}
  const lines = str.split('\n')
  for (const line of lines) {
    const entry = line.split('=')
    if (entry[0] && entry[1]) {
      const key = entry[0]
      const value = JSON.parse(entry[1])
      obj[key] = value
    }
  }
  return obj
}

function stringifyEnv (obj) {
  let str = ''
  for (const [key, value] of Object.entries(obj)) {
    const line = key + '=' + JSON.stringify(value) + '\n'
    str += line
  }
  return str
}
