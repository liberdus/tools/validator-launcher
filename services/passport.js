const passport = require('passport')
const JwtStragety = require('passport-jwt').Strategy
const LocalStrategy = require('passport-local')
const { ExtractJwt } = require('passport-jwt')
const { CONFIG } = require('./config')
const { USER } = require('./user')

// JWT Strategy setup
const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('token'),
  secretOrKey: CONFIG.secret
}

// Local Stragety Setup
const localOption = { usernameField: 'username' }
const localLogin = new LocalStrategy(
  localOption,
  (username, password, done) => {
    // verify username and password and call 'done' if correct
    let user = {
      username: username.toLowerCase(),
      password
    }
    USER.checkPassword(user).then(isMatch => {
      if (isMatch) done(null, user)
      else done(null, false)
    })
  }
)

const jwtLoginStrategy = new JwtStragety(jwtOptions, (payload, done) => {
  // check if user id in payload exist in database
  let foundUser = USER.getUser(payload.sub)
  if (foundUser) done(null, foundUser)
  else done(null, false)
})

passport.use(jwtLoginStrategy)
passport.use(localLogin)
