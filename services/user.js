const fs = require('fs')
const bcrypt = require('bcrypt-nodejs')
const { CONFIG_PATH } = require('./config')

class User {
  constructor(configPath) {
    this.configPath = configPath
    try {
      this.config = JSON.parse(fs.readFileSync(configPath, 'utf8'))
      this.accounts = this.config.accounts
    } catch (e) {
      console.log('user.js: Unable to create User instance:', e)
      this.accounts = []
    }
  }
  async addUser(user) {
    let existingUser = this.accounts.find((a) => a.username === user.username.toLowerCase())
    if (existingUser) {
      console.log('Username already existed')
      return false
    }
    let hashedUser = await this.hashPassword(user)
    this.accounts.push(hashedUser)

    this.config.accounts = this.accounts
    fs.writeFileSync(this.configPath, JSON.stringify(this.config, null, 2))

    return true
  }
  getUser(username) {
    let user = this.accounts.find((a) => a.username === username.toLowerCase())
    return user
  }
  deleteUser(username) {
    let userIdx = this.accounts.findIndex((a) => a.username === username.toLowerCase())
    if (userIdx < 0) return false
    this.accounts.splice(userIdx)
    return true
  }
  checkPassword(user) {
    let foundUser = this.getUser(user.username)
    if (!foundUser) {
      console.log('No user found')
      return false
    }

    let candidatePassword = user.password
    let hashedPassword = foundUser.password

    return new Promise((resolve) => {
      bcrypt.compare(candidatePassword, hashedPassword, (err, isMatch) => {
        if (err) {
          resolve(false)
        }
        resolve(isMatch)
      })
    })
  }
  hashPassword(inputUser) {
    let user = { ...inputUser }
    return new Promise((resolve, reject) => {
      bcrypt.genSalt(10, (err, salt) => {
        if (err) {
          console.log(err)
          return resolve(false)
        }
        bcrypt.hash(user.password, salt, null, (err, hash) => {
          if (err) {
            return false
          }
          user.password = hash
          resolve(user)
        })
      })
    })
  }
  changePassword(user) {
    const existingUser = this.getUser(user.username)
    if (!existingUser) {
      console.log('Username not found for password change')
      return false
    }
    // Delete old user
    if (this.deleteUser(user.username) === false) {
      console.log('Unable to delete old user')
      return false
    }
    // Create new user with new password
    this.addUser(user)
    return true
  }
}

/** CREATE DEFAULT USER */

let USER

if (!USER) {
  USER = new User(CONFIG_PATH)
  USER.addUser({
    username: process.env.USERNAME || 'admin',
    password: process.env.PASSWORD || 'liberdus',
  })
}

exports.USER = USER
