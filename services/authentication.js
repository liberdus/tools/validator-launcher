const jwt = require('jwt-simple')
const { CONFIG } = require('./config')
const { USER } = require('./user')

const generateTokenForUser = user => {
  const timestamp = new Date().getTime()
  return jwt.encode({ sub: user.username, iat: timestamp }, CONFIG.secret)
}

exports.signup = (req, res, next) => {
  const { username, password } = req.body
  if (!username || !password) {
    return res.status(422).send({ error: 'Pls provide username and password' })
  }
  let newUser = {
    username: username.toLowerCase(),
    password
  }
  let isSuccess = USER.addUser(newUser)
  if (isSuccess)
    return res.json({ success: true, token: generateTokenForUser(newUser) })
  else return res.json({ success: false })
}

exports.signin = (req, res) => {
  res.send({ token: generateTokenForUser(req.user) })
}

exports.changepass = (req, res) => {
  const { username, password } = req.body
  if (!username || !password) {
    return res.status(422).send({ error: 'Pls provide username and password' })
  }
  let changedUser = {
    username: username.toLowerCase(),
    password
  }
  let isSuccess = USER.changePassword(changedUser)
  if (isSuccess)
    return res.json({ success: true, token: generateTokenForUser(changedUser) })
  else return res.json({ success: false })
}