const fs = require('fs')
const path = require('path')
const DEFAULT_CONFIG = require('../launcher-config.default.json')

/** LOAD OR CREATE LAUNCHER-CONFIG IN BASE_DIR */

const BASE_DIR = process.argv[2] || process.cwd()
const LIBERDUS_PATH = process.argv[3]

const CONFIG_PATH = path.resolve(BASE_DIR, 'launcher-config.json')
let CONFIG
try {
  CONFIG = JSON.parse(fs.readFileSync(CONFIG_PATH, 'utf8'))
  if (!CONFIG.liberdus)
    throw new Error("launcher-config.json missing 'liberdus'")
  if (!CONFIG.port) throw new Error("launcher-config.json missing 'port'")
  if (!CONFIG.secret) throw new Error("launcher-config.json missing 'secret'")
  if (!CONFIG.accounts)
    throw new Error("launcher-config.json missing 'accounts'")
} catch (err) {
  const msg = err.message || err
  console.error(msg)
  console.log("Creating new 'launcher-config.json'...")

  let resolvedLiberdus
  try {
    resolvedLiberdus = require.resolve('liberdus')
  } catch (err) {}

  DEFAULT_CONFIG.liberdus =
    LIBERDUS_PATH || resolvedLiberdus || DEFAULT_CONFIG.liberdus

  fs.writeFileSync(CONFIG_PATH, JSON.stringify(DEFAULT_CONFIG, null, 2))

  CONFIG = JSON.parse(fs.readFileSync(CONFIG_PATH, 'utf8'))
}

exports.BASE_DIR = BASE_DIR
exports.CONFIG_PATH = CONFIG_PATH
exports.CONFIG = CONFIG
