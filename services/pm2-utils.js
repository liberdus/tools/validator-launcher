const fs = require('fs')
const path = require('path')
const execa = require('execa')
const fst = require('fs-tail-stream')
const { BASE_DIR } = require('./config')

const pm2 = path.resolve(require.resolve('pm2'), '../../.bin/pm2')
const PM2_HOME = path.resolve(BASE_DIR, '.pm2/')

exports.start = (script, name, env = {}, pm2Args = []) => {
  env.PM2_HOME = PM2_HOME
  const parsedPm2Args = pm2Args.map((arg) => arg.split('pm2')[1] || arg).join(' ')
  const execaCmd = `${pm2} start ${script} --name=${name} ${parsedPm2Args}`
  console.log('pm2Start', execaCmd)
  return execa.command(execaCmd, { env, stdio: [0, 1, 2] })
}

exports.stop = (arg, env = {}) => {
  env.PM2_HOME = PM2_HOME
  return execa.command(`${pm2} stop ${arg}`, { env, stdio: [0, 1, 2] })
}

exports.log = (arg, env = {}) => {
  env.PM2_HOME = PM2_HOME
  const proc = execa.command(`${pm2} log ${arg}`, { env, all: true })
  return proc.all
}

exports.kill = (env = {}) => {
  env.PM2_HOME = PM2_HOME
  return execa.command(`${pm2} kill`, { env, stdio: [0, 1, 2] })
}

exports.flush = (env = {}) => {
  env.PM2_HOME = PM2_HOME
  return execa.command(`${pm2} flush`, { env, stdio: [0, 1, 2] })
}

exports.reset = (arg, env = {}) => {
  env.PM2_HOME = PM2_HOME
  return execa.command(`${pm2} reset ${arg}`, { env, stdio: [0, 1, 2] })
}

exports.del = (arg, env = {}) => {
  env.PM2_HOME = PM2_HOME
  return execa.command(`${pm2} del ${arg}`, { env, stdio: [0, 1, 2] })
}

exports.list = async (env = {}) => {
  env.PM2_HOME = PM2_HOME
  const proc = await execa.command(`${pm2} list`, { env, all: true })
  return proc.all
}
exports.jlist = async (env = {}) => {
  env.PM2_HOME = PM2_HOME
  const proc = await execa.command(`${pm2} jlist`, { env, all: true })
  return proc.all
}

exports.exec = (arg, env = {}) => {
  env.PM2_HOME = PM2_HOME
  return execa.command(`${pm2} ${arg}`, { env, stdio: [0, 1, 2] })
}
